const {Schema, mongoose} = require('../database');

const schema = new Schema(
    {
        title: {
            type: String,
            required: true
        },
        body: {
            type: String
        }
    },
    {
        // дата создания и дата редоктирования
        timestamps: true
    }
);
//чтобы предстовляла в вде документа JSON
schema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Post', schema);